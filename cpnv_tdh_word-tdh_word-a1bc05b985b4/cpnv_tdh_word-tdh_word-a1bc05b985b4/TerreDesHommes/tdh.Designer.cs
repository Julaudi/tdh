﻿namespace TDH
{
    partial class tdh : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public tdh()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.grpReports = this.Factory.CreateRibbonGroup();
            this.cmdFraud = this.Factory.CreateRibbonButton();
            this.cmpPSE = this.Factory.CreateRibbonButton();
            this.cmdSecurity = this.Factory.CreateRibbonButton();
            this.cmdUpdate = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.grpReports.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.grpReports);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // grpReports
            // 
            this.grpReports.Items.Add(this.cmdFraud);
            this.grpReports.Items.Add(this.cmpPSE);
            this.grpReports.Items.Add(this.cmdSecurity);
            this.grpReports.Items.Add(this.cmdUpdate);
            this.grpReports.Label = "Rapports";
            this.grpReports.Name = "grpReports";
            // 
            // cmdFraud
            // 
            this.cmdFraud.Label = "Fraude";
            this.cmdFraud.Name = "cmdFraud";
            this.cmdFraud.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cmdFraud_Click);
            // 
            // cmpPSE
            // 
            this.cmpPSE.Label = "PSE";
            this.cmpPSE.Name = "cmpPSE";
            this.cmpPSE.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cmpPSE_Click);
            // 
            // cmdSecurity
            // 
            this.cmdSecurity.Label = "Sécurité";
            this.cmdSecurity.Name = "cmdSecurity";
            this.cmdSecurity.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cmdSecurity_Click);
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Label = "Mise à jour";
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cmdUpdate_Click);
            // 
            // tdh
            // 
            this.Name = "tdh";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.tdh_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpReports.ResumeLayout(false);
            this.grpReports.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpReports;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton cmdFraud;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton cmpPSE;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton cmdSecurity;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton cmdUpdate;
    }

    partial class ThisRibbonCollection
    {
        internal tdh tdh
        {
            get { return this.GetRibbon<tdh>(); }
        }
    }
}
