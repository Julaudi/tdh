﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;
using System.Windows.Forms;

namespace TDH
{
    class TDHFormField
    {
        private string _fieldNameForDisplay; // The name to use for display
        private string _stringVal;  // The raw value read as a string
        private bool _ok;           // Indicates if the field has been successfully loaded
        private bool _state;        // Indicates if the checkbox is checked or not
        private string c = "\u2612"; //unicode for checked checkbox dont ask...it works...
        /*
         * private string c = "\u2612";
         * Sadly I was unable to access the Checked property,
         * so this is a working abomination
         */

        /// <summary>
        /// Constructor.
        /// Initializes the object with the value of the field at the specified location (bookmark) and load it in the object
        /// The content of the field starts at the bookmark location and ends at the end of the bookmark
        /// </summary>
        /// <param name="doc">The Word document currently in use</param>
        /// <param name="bookmarkName">The name of the bookmark we're looking for</param>
        /// <param name="fieldNameForDisplay">The human-readable field name, used in case of errors</param>
        /// <param name="fieldType">The type of the field</param>
        public TDHFormField(Word.Document doc, string bookmarkName, string fieldNameForDisplay, string mandatoryPrefix, string fieldType = "string")
        {
            _ok = false;
            _fieldNameForDisplay = fieldNameForDisplay;

            foreach (Word.Bookmark bookmark in doc.Bookmarks)
            {
                if (fieldType == "activeXcheckbox")
                { //for ActiveX checkboxes, can be deleted as we won't be using them but I was asked not to modify anything apart from comments...
                    foreach (Word.InlineShape shape in doc.InlineShapes)
                    {
                        if (shape.OLEFormat != null && shape.OLEFormat.ClassType == "Forms.CheckBox.1" && shape.OLEFormat.Object.Name == bookmarkName)
                        {
                            _stringVal = shape.OLEFormat.Object.Value.ToString();
                            _ok = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (bookmark.Name.Replace(mandatoryPrefix, "") == bookmarkName)
                    {// This is the bookmark we're looking for
                        Word.Range result = doc.Range(bookmark.Start, bookmark.End); // The result contains the entire bookmark range
                        if (fieldType == "checkbox")
                        {
                            if (result.Text == c.ToString())
                            {
                                _state = true;
                            }
                            else
                            {
                                _state = false;
                            }
                        }
                        else if (fieldType == "dropdown")
                        {
                            _stringVal = result.Text;
                        }
                        else if (fieldType != "document")
                        {
                            _stringVal = result.Text.Remove(result.Text.Length - 1);
                        }

                        _ok = true;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// String getter
        /// </summary>
        /// <returns>Returns the value of the bookmark as a string</returns>
        public String getValue()
        {
            if (String.IsNullOrWhiteSpace(_stringVal))
            {
                return null;
            }
            else
            {
                return _stringVal;
            }
        }

        /// <summary>
        /// String getter
        /// </summary>
        /// <returns>Returns the fieldName of the bookmark as a string</returns>
        public String getFieldNameForDisplay()
        {
            return _fieldNameForDisplay;
        }

        /// <summary>
        /// Getter
        /// </summary>
        /// <returns>Returns the state of isOk</returns>
        public bool isOk()
        {
            return _ok;
        }

        /// <summary>
        /// Getter
        /// </summary>
        /// <returns>Returns the state of isChecked</returns>
        public bool isChecked()
        {
            return _state;
        }
    }
}
