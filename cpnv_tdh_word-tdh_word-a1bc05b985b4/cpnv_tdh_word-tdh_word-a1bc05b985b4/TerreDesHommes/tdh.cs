﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using WTools = Microsoft.Office.Tools.Word;
using System.Data;
using System.Data.Odbc;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace TDH
{
    public partial class tdh
    {
        bool send = true; //used for error checking, if any error is detected the request will not be sent to the database
        private const string MyConString = "DSN=TDH"; //name of the ODBC connection needed to send to the database
        string queryErrors = null; //for a further amelioration, instead of 15 MessageBoxes just one with all errors

        private List<Word.Bookmark> listOfMandatoryBookmark;//list containing the bookmarks detected as mandatory
        private List<string> listOfMandatoryBookmarkNameEmpty;//list containing the mandatory bookmarks detected as empty
        private string mandatoryPrefix = "man_";

        #region GUI Events
        private void tdh_Load(object sender, RibbonUIEventArgs e)
        {
        }

        private void cmdFraud_Click(object sender, RibbonControlEventArgs e)
        {
            send = true;
            // Handle on our document
            Word.Document TheDoc = Globals.ThisAddIn.Application.ActiveDocument;

            foreach (Word.Bookmark bookmark in TheDoc.Bookmarks)
            {
                if (bookmark.Name.Substring(0, mandatoryPrefix.Length-1) == mandatoryPrefix)
                {
                    if (listOfMandatoryBookmark == null)
                    {
                        listOfMandatoryBookmark = new List<Word.Bookmark>();
                    }
                    listOfMandatoryBookmark.Add(bookmark);
                }
            }

            // Check the document's template
            TDHFormField docVal = new TDHFormField(TheDoc, "Fraude", "Rapport de Fraude", mandatoryPrefix, "document");
            if (!docVal.isOk())
            {
                MessageBox.Show("Ce document ne semble pas être un rapport de fraude");
                return;
            }

            //Voici l'endroit ou nous voulons atteindre les valeurs des "Value" depuis les combos box.
            //Nous créeons la requete après cela

            string insertion = "use tdh insert into frauds ("
                            + "date_incident,"
                            + "lieu_incident,"
                            + "zone,"
                            + "pays,"
                            + "gravite_incident,"
                            + "responsabilite_tdh,"
                            + "resume_incident,"
                            + "nature_de_lincident_nature,"
                            + "nature_de_lincident_autre,"
                            + "nature_de_lincident_estimation_du_montant,"
                            + "details_de_lincident,"
                            + "preuves_significatives_signalement_superieur_hierarchique,"
                            + "preuves_significatives_signalement_superieur_hierarchique_date,"
                            + "preuves_significatives_une_implication_externe,"
                            + "preuves_significatives_une_implication_externe_date,"
                            + "preuves_significatives_avec_quelle_instance,"
                            + "preuves_significatives_si_autre,"
                            + "mesures_prises_par_la_delegation,"
                            + "recommandations_du_delegue_et_ou_de_la_personne_en_charge_des_finances,"
                            + "personnes_impliquees_expatrie,"
                            + "personnes_impliquees_consultant_prestataire_expatrie,"
                            + "personnes_impliquees_staff_national_tdh,"
                            + "personnes_impliquees_consultant_prestataire_local,"
                            + "personnes_impliquees_tiers,"
                            + "personnes_impliquees_nombre_total_de_personnes_impliquees,"
                            + "incident_demande_suivi,"
                            + "incident_demande_suivi_type,"
                            + "support_necessaire,"
                            + "support_necessaire_type,"
                            + "rapport_rempli_par,"
                            + "date_de_lenvoi_au_siege,"
                            + "rapport_traite_par,"
                            + "mise_a_jour_du_cas,"
                            + "mise_a_jour_du_cas_date,"
                            + "a_remplir_par_le_siege_numero_de_lincident,"
                            + "a_remplir_par_le_siege_recu_par,"
                            + "a_remplir_par_le_siege_recu_par_autre,"
                            + "a_remplir_par_le_siege_traite_au_siege_par,"
                            + "a_remplir_par_le_siege_montant_chf,"
                            + "a_remplir_par_le_siege_mise_a_jour_recommandation,"
                            + "a_remplir_par_le_siege_date_de_fermeture,"
                            + "a_remplir_par_le_siege_statut,"
                            + "annee,"
                            + "langue) "

                + "values ("
                    + "'" + getString(TheDoc, "date_incident", "Date de l'incident") + "'" + ", "//date_incident
                    + "'" + getString(TheDoc, "lieu_incident", "Lieu de l'incident") + "'" + ", "//lieu_incident
                    + "'" + getValeur(TheDoc, "zone") + "'" + ", " //zone
                    + "'" + getValeur(TheDoc, "pays") + "'" + ", "  //pays
                    + "'" + getValeur(TheDoc, "gravite") + "'" + ", " //Gravite_incident
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", "   //Responsabilite_tdh
                    + "'" + getTextBox(TheDoc, "ResumeIncident") + "'" + ", " //Resume_incident
                    + "'" + getValeur(TheDoc, "fraude_nature") + "'" + ", "//nature_de_lincident_nature
                    + "'" + getString(TheDoc, "nature_de_lincident_autre", "nature_de_lincident_autre") + "'" + ", "//nature_de_lincident_autre
                    + "'" + getString(TheDoc, "nature_de_lincident_estimation_du_montan", "nature_de_lincident_estimation_du_montant") + "'" + ", "//nature_de_lincident_estimation_du_montant
                    + "'" + getTextBox(TheDoc, "DetailsIncident") + "'" + ", " //Details_incident
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", "  //preuves_significatives_signalement_superieur_hierarchique
                    + "'" + getString(TheDoc, "preuves_significatives_signalement_date", "preuves_significatives_signalement_date") + "'" + ", " //preuves_significatives_signalement_superieur_hierarchique_date
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", "  //preuves_significatives_une_implication_externe
                    + "'" + getString(TheDoc, "preuves_significatives_implication_date", "preuves_significatives_implication_date") + "'" + ", "//preuves_significatives_une_implication_externe_date
                    + "'" + getValeur(TheDoc, "recours_instance") + "'" + ", "  //preuves_significatives_avec_quelle_instance
                    + "'" + getString(TheDoc, "preuves_significatives_si_autre", "preuves_significatives_si_autre") + "'" + ", "//preuves_significatives_si_autre
                    + "'" + getTextBox(TheDoc, "MesuresDelegation") + "'" + ", " //mesures_prises_par_la_delegation
                    + "'" + getTextBox(TheDoc, "Recommandations") + "'" + " ," //recommandations_du_delegue_et_ou_de_la_personne_en_charge_des_finances
                    + "'" + getString(TheDoc, "personnes_impliquees_expatrie", "personnes_impliquees_expatrie") + "'" + ", "//personnes_impliquees_expatrie
                    + "'" + getString(TheDoc, "personnes_impliquees_consultant_prestata", "personnes_impliquees_consultant_prestata") + "'" + ", "//personnes_impliquees_consultant_prestataire_expatrie
                    + "'" + getString(TheDoc, "personnes_impliquees_staff_national_tdh", "personnes_impliquees_staff_national_tdh") + "'" + ", "//personnes_impliquees_staff_national_tdh
                    + "'" + getString(TheDoc, "personnes_impliquees_consultant_prestat", "personnes_impliquees_consultant_prestat") + "'" + ", "//personnes_impliquees_consultant_prestataire_local
                    + "'" + getString(TheDoc, "personnes_impliquees_tiers", "personnes_impliquees_tiers") + "'" + ", "//personnes_impliquees_tiers
                    + "'" + getString(TheDoc, "personnes_impliquees_nombre_total_de_per", "personnes_impliquees_nombre_total_de_per") + "'" + ", "//personnes_impliquees_nombre_total_de_personnes_impliquees
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", "  //incident_demande_suivi
                    + "'" + getString(TheDoc, "incident_demande_suivi_type", "incident_demande_suivi_type") + "'" + ", "// incident_demande_suivi_type
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", "  //support_necessaire
                    + "'" + getString(TheDoc, "support_necessaire_type", "support_necessaire_type") + "'" + ", "//support_necessaire_type
                    + "'" + getString(TheDoc, "rapport_rempli_par", " rapport_rempli_par") + "'" + ", "// rapport_rempli_par
                    + "'" + getString(TheDoc, "date_de_lenvoi_au_siege", "date_de_lenvoi_au_siege") + "'" + ", "// date_de_lenvoi_au_siege
                    + "'" + getString(TheDoc, "rapport_traite_par", "rapport_traite_par") + "'" + ", "// rapport_traite_par
                    + "'" + getTextBox(TheDoc, "MAJCas") + "'" + ", " //mesures_prises_par_la_delegation
                    + "'" + getString(TheDoc, "mise_a_jour_du_cas_date", "mise_a_jour_du_cas_date") + "'" + ", "//mise_a_jour_du_cas_date
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_numero_de_lincide", "a_remplir_par_le_siege_numero_de_lincide") + "'" + ", "// a_remplir_par_le_siege_numero_de_lincident
                    + "'" + getValeur(TheDoc, "recu_par") + "'" + ", "  //a_remplir_par_le_siege_recu_par
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_recu_par_autre", "a_remplir_par_le_siege_recu_par_autre") + "'" + ", "// a_remplir_par_le_siege_recu_par_autre
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_traite_au_siege", "a_remplir_par_le_siege_traite_au_siege") + "'" + ", "// a_remplir_par_le_siege_traite_au_siege_par
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_montant_chf", "a_remplir_par_le_siege_montant_chf") + "'" + ", "// a_remplir_par_le_siege_montant_chf
                    + "'" + getTextBox(TheDoc, "MAJRecommandation") + "'" + "," //a_remplir_par_le_siege_mise_a_jour_recommandation
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_date_de_fermeture", "a_remplir_par_le_siege_date_de_fermeture") + "'" + ", "// a_remplir_par_le_siege_date_de_fermeture
                    + "'" + getValeur(TheDoc, "statut") + "'" + ", "//a_remplir_par_le_siege_statut
                    + "'" + getString(TheDoc, "date_incident", "date_de_incdient_pour_annee").Substring(0, 4) + "'" + ", "//annee
                    + "'" + int.Parse(getString(TheDoc, "Langue", "Langue")) + "'"//langue
                    + ")";
            if (send)
            {
                string reportReference = getString(TheDoc, "a_remplir_par_le_siege_numero_de_lincide", "a_remplir_par_le_siege_numero_de_lincide");// a_remplir_par_le_siege_numero_de_lincident
                ReportExists(reportReference, "frauds");
                sendToDatabase(insertion, TheDoc);
            }
            else
            {
                MessageBox.Show("Marche Pas");
            }

        }

        private void cmpPSE_Click(object sender, RibbonControlEventArgs e)
        {
            send = true;
            // Handle on our document
            Word.Document TheDoc = Globals.ThisAddIn.Application.ActiveDocument;

            // Check the document's template
            TDHFormField docVal = new TDHFormField(TheDoc, "PSE", "Rapport de PSE", mandatoryPrefix, "document");
            if (!docVal.isOk())
            {
                MessageBox.Show("Ce document ne semble pas être un rapport de PSE");
                return;
            }


            string insertion = "use tdh insert into pse ("
                + "date_incident,"
                + "lieu_incident,"
                + "zone,"
                + "pays,"
                + "gravite_incident,"
                + "responsabilite_tdh,"
                + "resume_incident,"
                + "violence_physique,"
                + "violence_physique_nature,"
                + "violence_physique_autre,"
                + "violence_sexuelle,"
                + "violence_sexuelle_nature,"
                + "violence_sexuelle_autre,"
                + "violence_psychologique,"
                + "violence_psychologique_nature,"
                + "violence_psychologique_autre,"
                + "negligence,"
                + "negligence_nature,"
                + "negligence_autre,"
                + "details_de_lincident,"
                + "victime_enfant_statut,"
                + "victime_enfant_nom,"
                + "victime_enfant_date_de_naissance,"
                + "victime_enfant_age,"
                + "victime_enfant_sexe,"
                + "victime_enfant_adresse,"
                + "victime_enfant_handicap,"
                + "victime_enfant_autre_information_importante,"
                + "agresseur_statut,"
                + "agresseur_nom,"
                + "agresseur_position,"
                + "agresseur_age,"
                + "agresseur_sexe,"
                + "agresseur_statut_familial,"
                + "agresseur_nom_de_lemployeur,"
                + "agresseur_lien_avec_tdh,"
                + "agresseur_lien_avec_lenfant,"
                + "agresseur_autre_information_importante,"
                + "personne_signalant_nom,"
                + "personne_signalant_position,"
                + "personne_signalant_adresse,"
                + "personne_signalant_nom_de_lemployeur,"
                + "personne_signalant_lien_avec_tdh,"
                + "personne_signalant_lien_avec_lenfant,"
                + "personne_signalant_autre_information_importante,"
                + "mesures_prises_pour_proteger_lenfant,"
                + "mesures_prises_pour_eviter_la_recidive,"
                + "mesures_prises_pour_proteger_laction_de_tdh,"
                + "informations_supplementaires_quest_ce_que_lenfant_souhaite,"
                + "informations_supplementaires_de_quel_soutien_lenfant_a_t_il_besoin,"
                + "informations_supplementaires_qui_doit_apporter_ce_soutien,"
                + "informations_supplementaires_y_a_t_il_dautres_actions,"
                + "informations_supplementaires_qui_quand,"
                + "informations_supplementaires_qui_va_suivre,"
                + "informations_supplementaires_lecons_apprises,"
                + "recommandations_du_delegue,"
                + "rapport_rempli_par,"
                + "date_de_lenvoi_au_siege,"
                + "rapport_traite_par,"
                + "mise_a_jour_du_cas,"
                + "mise_a_jour_du_cas_date,"
                + "a_remplir_par_le_siege_numero_de_lincident,"
                + "a_remplir_par_le_siege_recu_par,"
                + "a_remplir_par_le_siege_recu_par_autre,"
                + "a_remplir_par_le_siege_traite_au_siege_par,"
                + "a_remplir_par_le_siege_mise_a_jour_recommandation,"
                + "a_remplir_par_le_siege_date_de_fermeture,"
                + "a_remplir_par_le_siege_statut,"
                + "annee,"
                + "langue) "

                + "values ("

                    + "'" + getString(TheDoc, "date_incident", "Date") + "'" + ", " //date
                    + "'" + getString(TheDoc, "lieu_incident", "Lieu de l'incident") + "'" + ", " //lieu_incident
                    + "'" + getValeur(TheDoc, "zone") + "'" + ", " //zone
                    + "'" + getValeur(TheDoc, "pays") + "'" + ", "  //pays
                    + "'" + getValeur(TheDoc, "gravite") + "'" + ", " //Gravite_incident
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", "   //Responsabilite_tdh
                    + "'" + getTextBox(TheDoc, "ResumeIncident") + "'" + ", " //Resume_incident
                    + "'" + getCheckbox(TheDoc, "violence_physique", "Violence physique") + "'" + ", " //violence_physique
                    + "'" + getValeur(TheDoc, "violence_physique_nature") + "'" + ", " //violence_physique_nature
                    + "'" + getString(TheDoc, "violence_physique_autre", "Violence physique autre") + "'" + ", " //violence_physique_autre 
                    + "'" + getCheckbox(TheDoc, "violence_sexuelle", "Violence sexuelle") + "'" + ", " //violence_sexuelle
                    + "'" + getValeur(TheDoc, "violence_sexuelle_nature") + "'" + ", " //violence_sexuelle_nature
                    + "'" + getString(TheDoc, "violence_sexuelle_autre", "Violence sexuelle autre") + "'" + ", " //violence_sexuelle_autre
                    + "'" + getCheckbox(TheDoc, "violence_psychologique", "Violence psychologique") + "'" + ", " //violence_psychologique
                    + "'" + getValeur(TheDoc, "violence_psychologique_nature") + "'" + ", " //violence_psychologique_nature
                    + "'" + getString(TheDoc, "violence_psychologique_autre", "Violence psychologique autre") + "'" + ", " //violence_psychologique_autre
                    + "'" + getCheckbox(TheDoc, "negligence", "Néegligence") + "'" + ", " //negligence
                    + "'" + getValeur(TheDoc, "negligence") + "'" + ", " //negligence_nature
                    + "'" + getString(TheDoc, "negligence_autre", "Négligence autre") + "'" + ", " //negligence_autre
                    + "'" + getTextBox(TheDoc, "DetailsIncident") + "'" + ", " //details_de_lincident
                    + "'" + getValeur(TheDoc, "victime_statut") + "'" + ", " //victime_enfant_statut
                    + "'" + getString(TheDoc, "victime_enfant_nom", "Nom de l'enfant") + "'" + ", " //victime_enfant_nom
                    + "'" + getString(TheDoc, "victime_enfant_date_de_naissance", "Date de naissance de l'enfant") + "'" + ", " //victime_enfant_date_de_naissance
                    + "'" + getInt(TheDoc, "victime_enfant_age", "Age de la vîctime") + "'" + ", " //victime_enfant_age
                    + "'" + getValeur(TheDoc, "sexe") + "'" + ", " //victime_enfant_sexe
                    + "'" + getString(TheDoc, "victime_enfant_adresse", "Adresse de la victime") + "'" + ", " //victime_enfant_adresse
                    + "'" + getString(TheDoc, "victime_enfant_handicap", "Handicap victime") + "'" + ", " //victime_enfant_handicap
                    + "'" + getString(TheDoc, "victime_enfant_autre_information_importa", "Autre informations concernant la victime") + "'" + ", " //victime_enfant_autre_information_importante
                    + "'" + getValeur(TheDoc, "agresseur_statut") + "'" + ", " //agresseur_statut
                    + "'" + getString(TheDoc, "agresseur_nom", "Nom de l'agresseur") + "'" + ", " //agresseur_nom
                    + "'" + getString(TheDoc, "agresseur_position", "Position de l'agresseur") + "'" + ", " //agresseur_position
                    + "'" + getInt(TheDoc, "agresseur_age", "Age de l'agresseur") + "'" + ", " //agresseur_age
                    + "'" + getValeur(TheDoc, "sexe") + "'" + ", " //agresseur_sexe
                    + "'" + getString(TheDoc, "agresseur_statut_familial", "Statut familial de l'agresseur") + "'" + ", " //agresseur_statut_familial
                    + "'" + getString(TheDoc, "agresseur_nom_de_lemployeur", "Nom de l'employeur de l'agresseur") + "'" + ", " //agresseur_nom_de_lemployeur
                    + "'" + getString(TheDoc, "agresseur_lien_avec_tdh", "Lien de l'agresseur avec Tdh") + "'" + ", " //agresseur_lien_avec_tdh
                    + "'" + getString(TheDoc, "agresseur_lien_avec_lenfant", "Lien de l'agresseur avec l'enfant") + "'" + ", " //agresseur_lien_avec_lenfant
                    + "'" + getString(TheDoc, "agresseur_autre_information_importante", "Autre information concernant l'agresseur") + "'" + ", " //agresseur_autre_information_importante
                    + "'" + getString(TheDoc, "personne_signalant_nom", "Nom de la personne signalant") + "'" + ", " //personne_signalant_nom
                    + "'" + getString(TheDoc, "personne_signalant_position", "Position de la personne signalant") + "'" + ", " //personne_signalant_position
                    + "'" + getString(TheDoc, "personne_signalant_adresse", "Adresse de la personne signalant") + "'" + ", " //personne_signalant_adresse
                    + "'" + getString(TheDoc, "personne_signalant_nom_de_lemployeur", "Nom de l'employeur de la personne signalant") + "'" + ", " //personne_signalant_nom_de_lemployeur
                    + "'" + getString(TheDoc, "personne_signalant_lien_avec_tdh", "Lien avec Tdh de la personne signalant") + "'" + ", " //personne_signalant_lien_avec_tdh
                    + "'" + getString(TheDoc, "personne_signalant_lien_avec_lenfant", "Lien avec l'enfant de la personne signalant") + "'" + ", " //personne_signalant_lien_avec_lenfant
                    + "'" + getString(TheDoc, "personne_signalant_autre_information_imp", "Autre information concernant la personne signalant") + "'" + ", " //personne_signalant_autre_information_importante
                    + "'" + getTextBox(TheDoc, "MesureProtectionEnfant") + "'" + ", " //mesures_prises_pour_proteger_lenfant
                    + "'" + getTextBox(TheDoc, "MesuresRecidive") + "'" + ", " //mesures_prises_pour_eviter_la_recidive
                    + "'" + getTextBox(TheDoc, "MesuresProtectionTdh") + "'" + ", " //mesures_prises_pour_proteger_laction_de_tdh
                    + "'" + getTextBox(TheDoc, "EnfantSouhaite") + "'" + ", " //informations_supplementaires_quest_ce_que_lenfant_souhaite
                    + "'" + getTextBox(TheDoc, "EnfantBesoin") + "'" + ", " //informations_supplementaires_de_quel_soutien_lenfant_a_t_il_besoin
                    + "'" + getTextBox(TheDoc, "QuiQuandSoutien") + "'" + ", " //informations_supplementaires_qui_doit_apporter_ce_soutien
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", " //informations_supplementaires_y_a_t_il_dautres_actions
                    + "'" + getTextBox(TheDoc, "AutreQuiQuand") + "'" + ", " //informations_supplementaires_qui_quand
                    + "'" + getTextBox(TheDoc, "QuiSuitCas") + "'" + ", " //informations_supplementaires_qui_va_suivre
                    + "'" + getTextBox(TheDoc, "LeconsApprises") + "'" + ", " //informations_supplementaires_lecons_apprises
                    + "'" + getTextBox(TheDoc, "Recommandations") + "'" + ", " //recommandations_du_delegue
                    + "'" + getString(TheDoc, "rapport_rempli_par", " rapport_rempli_par") + "'" + ", "// rapport_rempli_par
                    + "'" + getString(TheDoc, "date_de_lenvoi_au_siege", "date_de_lenvoi_au_siege") + "'" + ", "// date_de_lenvoi_au_siege
                    + "'" + getString(TheDoc, "rapport_traite_par", "rapport_traite_par") + "'" + ", "// rapport_traite_par
                    + "'" + getTextBox(TheDoc, "MAJCas") + "'" + ", " //mesures_prises_par_la_delegation
                    + "'" + getString(TheDoc, "mise_a_jour_du_cas_date", "mise_a_jour_du_cas_date") + "'" + ", "//mise_a_jour_du_cas_date
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_numero_de_lincide", "a_remplir_par_le_siege_numero_de_lincident") + "'" + ", "// a_remplir_par_le_siege_numero_de_lincident
                    + "'" + getValeur(TheDoc, "recu_par") + "'" + ", "  //a_remplir_par_le_siege_recu_par
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_recu_par_autre", "a_remplir_par_le_siege_recu_par_autre") + "'" + ", "// a_remplir_par_le_siege_recu_par_autre
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_traite_au_siege_p", "a_remplir_par_le_siege_traite_au_siege") + "'" + ", "// a_remplir_par_le_siege_traite_au_siege_par
                    + "'" + getTextBox(TheDoc, "MAJRecommandation") + "'" + "," //a_remplir_par_le_siege_mise_a_jour_recommandation
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_date_de_fermeture", "a_remplir_par_le_siege_date_de_fermeture") + "'" + ", "// a_remplir_par_le_siege_date_de_fermeture
                    + "'" + getValeur(TheDoc, "statut") + "'" + ", "//a_remplir_par_le_siege_statut
                    + "'" + getString(TheDoc, "date_incident", "date_de_incdient_pour_annee").Substring(0, 4) + "'" + ", "//annee
                    + "'" + int.Parse(getString(TheDoc, "Langue", "Langue")) + "'"//langue
                    + ")";




            if (send)
            {
                string reportReference = getString(TheDoc, "a_remplir_par_le_siege_numero_de_lincide", "a_remplir_par_le_siege_numero_de_lincide");// a_remplir_par_le_siege_numero_de_lincident
                ReportExists(reportReference, "pse");
                sendToDatabase(insertion, TheDoc);
            }
            else
            {
                MessageBox.Show("Marche Pas");
            }
        }

        private void cmdSecurity_Click(object sender, RibbonControlEventArgs e)
        {
            send = true;
            // Handle on our document
            Word.Document TheDoc = Globals.ThisAddIn.Application.ActiveDocument;

            // Check the document's template
            TDHFormField docVal = new TDHFormField(TheDoc, "secu", "Rapport de Securité", mandatoryPrefix, "document");
            if (!docVal.isOk())
            {
                MessageBox.Show("Ce document ne semble pas être un rapport de securité");
                return;
            }

            string insertion = "use tdh insert into securities ("
                + "date_incident,"
                + "lieu_incident,"
                + "zone,"
                + "pays,"
                + "gravite_incident,"
                + "responsabilite_tdh,"
                + "resume_incident,"
                + "sante,"
                + "sante_employe,"
                + "sante_nature,"
                + "sante_autre,"
                + "accident_lie_au_deplacement,"
                + "accident_lie_au_deplacement_nature,"
                + "accident_lie_au_deplacement_autre,"
                + "maintenance_du_bureau,"
                + "maintenance_du_bureau_nature,"
                + "maintenance_du_bureau_autre,"
                + "catastrophe_naturelle,"
                + "catastrophe_naturelle_nature,"
                + "catastrophe_naturelle_autre,"
                + "degat_materiel,"
                + "degat_materiel_nature,"
                + "degat_materiel_autre,"
                + "surete_autre,"
                + "surete_autre_description,"
                + "menace,"
                + "menace_envers,"
                + "menace_envers_autre,"
                + "menace_nature,"
                + "menace_nature_autre,"
                + "agression,"
                + "agression_employe,"
                + "agression_nature,"
                + "agression_autre,"
                + "enlevement,"
                + "enlevement_employe,"
                + "enlevement_nature,"
                + "enlevement_autre,"
                + "vol_intrusion,"
                + "vol_intrusion_nature,"
                + "vol_intrusion_autre,"
                + "attaque,"
                + "attaque_nature,"
                + "attaque_autre,"
                + "troubles_publics,"
                + "troubles_publics_nature,"
                + "troubles_publics_autre,"
                + "securite_autre,"
                + "securite_autre_desctiption,"
                + "details_de_lincident,"
                + "raisons_de_la_presence_de_tdh,"
                + "mesures_prises_par_la_delegation,"
                + "recommandations_du_delegue,"
                + "personnes_impliquees_expatrie,"
                + "personnes_impliquees_consultant_prestataire_expatrie,"
                + "personnes_impliquees_staff_national_tdh,"
                + "personnes_impliquees_consultant_prestataire_local,"
                + "personnes_impliquees_tiers,"
                + "personnes_impliquees_nombre_total_de_personnes,"
                + "implication_exterieure_police,"
                + "implication_exterieure_juge_de_paix,"
                + "implication_exterieure_tiers_population,"
                + "implication_exterieure_hopital_centre_de_sante,"
                + "implication_exterieure_compagnie_dassurance,"
                + "implication_exterieure_aucune,"
                + "implication_exterieure_autre,"
                + "implication_exterieure_autre_description,"
                + "impact_sur_le_programme_hibernation,"
                + "impact_sur_le_programme_nb_de_jours,"
                + "impact_sur_le_programme_evacuation,"
                + "impact_sur_le_programme_nb_de_personnes,"
                + "impact_sur_le_programme_evacuation_medicale,"
                + "impact_sur_le_programme_nb_de_jours_sans_travailler,"
                + "impact_sur_le_programme_cout_financier,"
                + "impact_sur_le_programme_montant_approximatif,"
                + "impact_sur_le_programme_suivi_judicaire,"
                + "impact_sur_le_programme_autre,"
                + "impact_sur_le_programme_aucun,"
                + "impact_sur_le_programme_autre_description,"
                + "incident_demande_suivi,"
                + "incident_demande_suivi_type,"
                + "support_necessaire,"
                + "support_necessaire_type,"
                + "plan_securite_change,"
                + "plan_securite_envoyer_au_siege,"
                + "plan_securite_envoyer_au_siege_date,"
                + "rapport_rempli_par,"
                + "date_de_lenvoi_au_siege,"
                + "rapport_traite_par,"
                + "mise_a_jour_du_cas,"
                + "mise_a_jour_du_cas_date,"
                + "a_remplir_par_le_siege_numero_de_lincident ,"
                + "a_remplir_par_le_siege_recu_par,"
                + "a_remplir_par_le_siege_recu_par_autre,"
                + "a_remplir_par_le_siege_traite_au_siege_par,"
                + "a_remplir_par_le_siege_mise_a_jour_recommandation,"
                + "a_remplir_par_le_siege_statut,"
                + "a_remplir_par_le_siege_date_de_fermeture,"
                + "annee,"
                + "langue) "
                + "values ("
                    + "'" + getString(TheDoc, "date_incident", "Date de l'incident") + "'" + ", " //date_incident
                    + "'" + getString(TheDoc, "lieu_incident", "Lieu de l'incident") + "'" + ", " //lieu_incident
                    + "'" + getValeur(TheDoc, "zone") + "'" + ", " //zone
                    + "'" + getValeur(TheDoc, "pays") + "'" + ", " //pays
                    + "'" + getValeur(TheDoc, "gravite") + "'" + ", " //gravite_incident
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", " //responsabilite_tdh
                    + "'" + getTextBox(TheDoc, "ResumeIncident") + "'" + ", " //resume_incident
                    + "'" + getCheckbox(TheDoc, "sante", "santé_checkbox") + "'" + ", " //resume_incident
                    + "'" + getValeur(TheDoc, "sante_employeur") + "'" + ", " //sante_employe
                    + "'" + getValeur(TheDoc, "sante_nature") + "'" + ", " //sante_nature
                    + "'" + getString(TheDoc, "sante_autre", "sante_autre") + "'" + ", " //sante_autre
                    + "'" + getCheckbox(TheDoc, "accident_lie_au_deplacement", "accident_lie_au_deplacement_checkbox") + "'" + ", " //accident_lie_au_deplacement
                    + "'" + getValeur(TheDoc, "accident_deplacement_nature") + "'" + ", " //accident_lie_au_deplacement_nature
                    + "'" + getString(TheDoc, "accident_lie_au_deplacement_autre", "accident_lie_au_deplacement_autre") + "'" + ", " //accident_lie_au_deplacement_autre
                    + "'" + getCheckbox(TheDoc, "maintenance_du_bureau", "maintenance_du_bureau_checkbox") + "'" + ", " //maintenance_du_bureau
                    + "'" + getValeur(TheDoc, "maintenance_bureau") + "'" + ", " //maintenance_du_bureau_nature
                    + "'" + getString(TheDoc, "maintenance_du_bureau_autre", "maintenance_du_bureau_autre") + "'" + ", " //maintenance_du_bureau_autre
                    + "'" + getCheckbox(TheDoc, "catastrophe_naturelle", "catastrophe_naturelle_checkbox") + "'" + ", " //catastrophe_naturelle
                    + "'" + getValeur(TheDoc, "catastrophe_naturelle") + "'" + ", " //catastrophe_naturelle_nature
                    + "'" + getString(TheDoc, "catastrophe_naturelle_autre", "catastrophe_naturelle_autre") + "'" + ", " //catastrophe_naturelle_autre
                    + "'" + getCheckbox(TheDoc, "degat_materiel", "degat_materiel_checkbox") + "'" + ", " //degat_materiel
                    + "'" + getValeur(TheDoc, "degat_materiel") + "'" + ", " //degat_materiel_nature
                    + "'" + getString(TheDoc, "degat_materiel_autre", "degat_materiel_autre") + "'" + ", " //degat_materiel_autre
                    + "'" + getString(TheDoc, "surete_autre", "surete_autre") + "'" + ", " //surete_autre
                    + "'" + getString(TheDoc, "surete_autre_description", "surete_autre_description") + "'" + ", " //surete_autre_description
                    + "'" + getCheckbox(TheDoc, "menace", "menace_checkbox") + "'" + ", " //menace
                    + "'" + getValeur(TheDoc, "menace_envers") + "'" + ", " //menace_envers
                    + "'" + getString(TheDoc, "menace_envers_autre", "catastrophe_naturelle_autre") + "'" + ", " //catastrophe_naturelle_autre
                    + "'" + getValeur(TheDoc, "menace_nature") + "'" + ", " //menace_nature
                    + "'" + getString(TheDoc, "menace_nature_autre", "menace_nature_autre") + "'" + ", " //menace_nature_autre
                    + "'" + getCheckbox(TheDoc, "agression", "agression_checkbox") + "'" + ", " //agression
                    + "'" + getValeur(TheDoc, "sante_employe") + "'" + ", " //agression_employe
                    + "'" + getValeur(TheDoc, "agression_nature") + "'" + ", " //agression_nature
                    + "'" + getString(TheDoc, "agression_autre", "agression_autre") + "'" + ", " //agression_autre
                    + "'" + getCheckbox(TheDoc, "enlevement", "enlevement_checkbox") + "'" + ", " //enlevement
                    + "'" + getValeur(TheDoc, "sante_employe") + "'" + ", " //enlevement_employe
                    + "'" + getValeur(TheDoc, "enlevement_nature") + "'" + ", " //enlevement_nature
                    + "'" + getString(TheDoc, "enlevement_autre", "enlevement_autre") + "'" + ", " //enlevement_autre
                    + "'" + getCheckbox(TheDoc, "vol_intrusion", "vol_intrusion_checkbox") + "'" + ", " //vol_intrusion
                    + "'" + getValeur(TheDoc, "vol_intrusion_nature") + "'" + ", " //vol_intrusion_nature
                    + "'" + getString(TheDoc, "vol_intrusion_autre", "vol_intrusion_autre") + "'" + ", " //vol_intrusion_autre
                    + "'" + getCheckbox(TheDoc, "enlevement", "enlevement_checkbox") + "'" + ", " //attaque
                    + "'" + getValeur(TheDoc, "attaque_nature") + "'" + ", " //attaque_nature
                    + "'" + getString(TheDoc, "attaque_autre", "attaque_autre") + "'" + ", " //attaque_autre
                    + "'" + getCheckbox(TheDoc, "troubles_publics", "troubles_publics_checkbox") + "'" + ", " //troubles_publics
                    + "'" + getValeur(TheDoc, "troubles_publics_nature") + "'" + ", " //troubles_publics_nature
                    + "'" + getString(TheDoc, "troubles_publics_autre", "troubles_publics_autre") + "'" + ", " //troubles_publics_autre
                    + "'" + getString(TheDoc, "securite_autre", "securite_autre") + "'" + ", " //securite_autre
                    + "'" + getString(TheDoc, "securite_autre_desctiption", "securite_autre_desctiption") + "'" + ", " //securite_autre_desctiption
                    + "'" + getTextBox(TheDoc, "DetailsIncident") + "'" + ", " //details_de_lincident
                    + "'" + getTextBox(TheDoc, "RaisonPresence") + "'" + ", " //raisons_de_la_presence_de_tdh
                    + "'" + getTextBox(TheDoc, "MesuresDelegation") + "'" + ", " //mesures_prises_par_la_delegation
                    + "'" + getTextBox(TheDoc, "Recommandations") + "'" + ", " //recommandations_du_delegue
                    + "'" + getString(TheDoc, "personnes_impliquees_expatrie", "personnes_impliquees_expatrie") + "'" + ", " //personnes_impliquees_expatrie
                    + "'" + getString(TheDoc, "personnes_impliquees_consultant_pre_expa", "personnes_impliquees_consultant_pre_expa") + "'" + ", " //personnes_impliquees_consultant_prestataire_expatrie
                    + "'" + getString(TheDoc, "personnes_impliquees_staff_national_tdh", "personnes_impliquees_staff_national_tdh") + "'" + ", " //personnes_impliquees_staff_national_tdh
                    + "'" + getString(TheDoc, "personnes_impliquees_consultant_pre_loca", "personnes_impliquees_consultant_pre_loca") + "'" + ", " //personnes_impliquees_consultant_prestataire_local
                    + "'" + getString(TheDoc, "personnes_impliquees_tiers", "personnes_impliquees_tiers") + "'" + ", " //personnes_impliquees_tiers
                    + "'" + getString(TheDoc, "personnes_impliquees_nombre_total_de_per", "personnes_impliquees_nombre_total_de_per") + "'" + ", " //personnes_impliquees_nombre_total_de_personnes
                    + "'" + getCheckbox(TheDoc, "implication_exterieure_police", "implication_exterieure_police") + "'" + ", " //implication_exterieure_police
                    + "'" + getCheckbox(TheDoc, "implication_exterieure_juge_de_paix", "implication_exterieure_juge_de_paix") + "'" + ", " //implication_exterieure_juge_de_paix
                    + "'" + getCheckbox(TheDoc, "implication_exterieure_tiers_population", "implication_exterieure_tiers_population") + "'" + ", " //implication_exterieure_tiers_population
                    + "'" + getCheckbox(TheDoc, "implication_exterieure_hopital_centre_de", "implication_exterieure_hopital_centre_de") + "'" + ", " //implication_exterieure_hopital_centre_de_sante
                    + "'" + getCheckbox(TheDoc, "implication_exterieure_compagnie_dassura", "implication_exterieure_compagnie_dassura") + "'" + ", " //implication_exterieure_compagnie_dassurance
                    + "'" + getCheckbox(TheDoc, "implication_exterieure_aucune", "implication_exterieure_aucune") + "'" + ", " //implication_exterieure_aucune
                    + "'" + getCheckbox(TheDoc, "implication_exterieure_autre", "implication_exterieure_autre") + "'" + ", " //implication_exterieure_autre
                    + "'" + getString(TheDoc, "implication_exterieure_autre_description", "implication_exterieure_autre_description") + "'" + ", " //implication_exterieure_autre_description
                    + "'" + getCheckbox(TheDoc, "impact_sur_le_programme_hibernation", "impact_sur_le_programme_hibernation") + "'" + ", " //impact_sur_le_programme_hibernation
                    + "'" + getInt(TheDoc, "impact_sur_le_programme_nb_de_jours", "impact_sur_le_programme_nb_de_jours") + "'" + ", " //impact_sur_le_programme_nb_de_jours
                    + "'" + getCheckbox(TheDoc, "impact_sur_le_programme_evacuation", "impact_sur_le_programme_evacuation") + "'" + ", " //impact_sur_le_programme_evacuation
                    + "'" + getInt(TheDoc, "impact_sur_le_programme_nb_de_personnes", "impact_sur_le_programme_nb_de_personnes") + "'" + ", " //impact_sur_le_programme_nb_de_personnes
                    + "'" + getCheckbox(TheDoc, "impact_sur_le_programme_evacuation_medic", "impact_sur_le_programme_evacuation_medic") + "'" + ", " //impact_sur_le_programme_evacuation_medicale
                    + "'" + getInt(TheDoc, "impact_sur_le_programme_nb_de_jours_sans", "impact_sur_le_programme_nb_de_jours_sans") + "'" + ", " //impact_sur_le_programme_nb_de_jours_sans
                    + "'" + getCheckbox(TheDoc, "impact_sur_le_programme_cout_financier", "impact_sur_le_programme_cout_financier") + "'" + ", " //impact_sur_le_programme_cout_financier
                    + "'" + getString(TheDoc, "impact_sur_le_programme_montant_approxim", "impact_sur_le_programme_montant_approxim") + "'" + ", " //impact_sur_le_programme_montant_approximatif
                    + "'" + getCheckbox(TheDoc, "impact_sur_le_programme_suivi_judicaire", "impact_sur_le_programme_suivi_judicaire") + "'" + ", " //impact_sur_le_programme_suivi_judicaire
                    + "'" + getCheckbox(TheDoc, "impact_sur_le_programme_autre", "impact_sur_le_programme_autre") + "'" + ", " //impact_sur_le_programme_autre
                    + "'" + getCheckbox(TheDoc, "impact_sur_le_programme_aucun", "impact_sur_le_programme_aucun") + "'" + ", " //impact_sur_le_programme_aucun
                    + "'" + getString(TheDoc, "impact_sur_le_programme_autre_descriptio", "impact_sur_le_programme_autre_descriptio") + "'" + ", " //impact_sur_le_programme_autre_description
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", " //incident_demande_suivi
                    + "'" + getString(TheDoc, "incident_demande_suivi_type", "incident_demande_suivi_type") + "'" + ", " //incident_demande_suivi_type
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", " //support_necessaire
                    + "'" + getString(TheDoc, "support_necessaire_type", "support_necessaire_type") + "'" + ", " //support_necessaire_type
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", " //plan_securite_change
                    + "'" + getValeur(TheDoc, "oui_non") + "'" + ", " //vol_intrusion_nature
                    + "'" + getString(TheDoc, "plan_securite_envoyer_au_siege_date", "plan_securite_envoyer_au_siege_date") + "'" + ", " //plan_securite_envoyer_au_siege_date
                    + "'" + getString(TheDoc, "rapport_rempli_par", " rapport_rempli_par") + "'" + ", "// rapport_rempli_par
                    + "'" + getString(TheDoc, "date_de_lenvoi_au_siege", "date_de_lenvoi_au_siege") + "'" + ", "// date_de_lenvoi_au_siege
                    + "'" + getString(TheDoc, "rapport_traite_par", "rapport_traite_par") + "'" + ", "// rapport_traite_par
                    + "'" + getTextBox(TheDoc, "MAJCas") + "'" + ", " //mise_a_jour_du_cas
                    + "'" + getString(TheDoc, "mise_a_jour_du_cas_date", "mise_a_jour_du_cas_date") + "'" + ", "//mise_a_jour_du_cas_date
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_numero_de_lincide", "a_remplir_par_le_siege_numero_de_lincide") + "'" + ", "// a_remplir_par_le_siege_numero_de_lincident
                    + "'" + getValeur(TheDoc, "recu_par") + "'" + ", "  //a_remplir_par_le_siege_recu_par
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_recu_par_autre", "a_remplir_par_le_siege_recu_par_autre") + "'" + ", "// a_remplir_par_le_siege_recu_par_autre
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_traite_au_siege_p", "a_remplir_par_le_siege_traite_au_siege_p") + "'" + ", "// a_remplir_par_le_siege_traite_au_siege_par
                    + "'" + getTextBox(TheDoc, "MAJRecommandation") + "'" + "," //a_remplir_par_le_siege_mise_a_jour_recommandation
                    + "'" + getValeur(TheDoc, "statut") + "'" + ", "  //a_remplir_par_le_siege_statut
                    + "'" + getString(TheDoc, "a_remplir_par_le_siege_date_de_fermeture", "a_remplir_par_le_siege_date_de_fermeture") + "'" + ", "// a_remplir_par_le_siege_date_de_fermeture
                    + "'" + getString(TheDoc, "date_incident", "date_de_incdient_pour_annee").Substring(0, 4) + "'" + ", "//annee
                    + "'" + int.Parse(getString(TheDoc, "Langue", "Langue")) + "'"//langue
                    + ")";


            if (send)
            {
                string reportReference = getString(TheDoc, "a_remplir_par_le_siege_numero_de_lincide", "a_remplir_par_le_siege_numero_de_lincide");// a_remplir_par_le_siege_numero_de_lincident
                ReportExists(reportReference, "securities");
                sendToDatabase(insertion, TheDoc);
            }
            else
            {
                MessageBox.Show("Marche pas");
            }
        }

        //Boutton permettant de mettre les listes déroulante à jour depuis les données de la base de donnée.
        //Une connection SQL est utilisée afin de faire une requete peremttant de trouver toutes les traductions dans la langue voulue et les rnager par listes.
        //Les indexs sont tous parcourus et uniquement les Liste déroulantes sont traitée.
        //La langue est prise depuis le document (Un caractere invisible) Il faudra peut-être changer cela
        //
        private void cmdUpdate_Click(object sender, RibbonControlEventArgs e) // XCL: This is where we will read the dropdown value (zone selected)
        {
            Word.Document TheDoc = Globals.ThisAddIn.Application.ActiveDocument;
            int j = 1;
            int langue = int.Parse(getString(TheDoc, "Langue", "Langue"));

            this.cmdUpdate.Visible = false;
            OdbcConnection trouveListe = new OdbcConnection(MyConString);
            trouveListe.Open();
            for (int i = 1; i <= TheDoc.ContentControls.Count; i++)
            {
                if (TheDoc.ContentControls[i].Type == Word.WdContentControlType.wdContentControlComboBox)
                {
                    TheDoc.ContentControls[i].DropdownListEntries.Clear();
                    string multi = "SELECT Traduction, Valeur FROM Traductions INNER JOIN Listes ON FK_Listes = IDlistes WHERE Nom_liste ='" + TheDoc.ContentControls[i].Tag + "' AND FK_Langues ='" + langue + "'";
                    using (OdbcCommand cmd = new OdbcCommand(multi, trouveListe))
                    {
                        OdbcDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                TheDoc.ContentControls[i].DropdownListEntries.Add(reader.GetString(0), reader.GetString(1));
                            }
                        }
                        reader.Close();
                    }
                }
            }
        }
        #endregion GUI Events

        #region communication with database
        public void sendToDatabase(string query, Word.Document TheDoc)
        {
            //MessageBox.Show(query); //for logging/debugging purposes only
            OdbcConnection MyConn = new OdbcConnection(MyConString);
            MyConn.Open();
            OdbcCommand command = new OdbcCommand(query, MyConn);
            using (OdbcConnection connection = new OdbcConnection(MyConString))
            {
                command.Connection = connection;
                connection.Open();
                command.ExecuteNonQuery();
                MessageBox.Show("Le document a été correcement envoyé dans la base de donnée.");
                MyConn.Close();
                //TheDoc.SaveAs2("c:/DansDB/" + TheDoc.Name.ToString().Replace(".docx", "") + "(Dans DB).docx");
            }
        }

        //
        private void ReportExists(string reportReference, string tableName)
        {
            //built query
            string identifier = "a_remplir_par_le_siege_numero_de_lincident";
            string query = "SELECT COUNT('" + identifier + "') FROM '" + tableName + "' WHERE '" + identifier + "' LIKE '" + reportReference + "'";

            OdbcConnection MyConn = new OdbcConnection(MyConString);
            MyConn.Open();
            OdbcCommand command = new OdbcCommand(query, MyConn);
            using (OdbcConnection connection = new OdbcConnection(MyConString))
            {
                command.Connection = connection;
                try
                {
                    connection.Open();
                    int queryAnswer = (int)command.ExecuteScalar();
                    MyConn.Close();
                    if (queryAnswer > 0)
                    {
                        MessageBox.Show("Un rapport avec la référence " + reportReference + " a été trouvé dans la base de données", "Risque de doublon");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Impossible de vérifier si un rapport avec la référence " + reportReference + " est déjà présent dans la base de données", "Risque de doublon");
                }
            }
        }

        #endregion communication with database

        #region Word accessors (get fields value)
        /// <summary>
        /// Get the date from bookmark in the Word document
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <param name="bookmark">The bookmark of the date we wanna retreive</param>
        /// <param name="field">The name of the field we are retreiving, used solely for error displaying</param>
        /// <returns>Returns the date as a string in the yyyy-mm-dd format</returns>
        public string getDate(Word.Document TheDoc, string bookmark, string field)
        {            
            //for now, if jan or janv is entered instead of 1, the addin will crash
            string year = null, month = null, day = null;

            TDHFormField[] yearField = { new TDHFormField(TheDoc, bookmark+"_year", mandatoryPrefix, "Year"),
                                        new TDHFormField(TheDoc, bookmark+"_month", mandatoryPrefix, "Month"),
                                        new TDHFormField(TheDoc, bookmark+"_day", mandatoryPrefix, "Day") };

            foreach (TDHFormField f in yearField)
            { // Let's see which field is checked
                if (f.isOk() && !String.IsNullOrWhiteSpace(f.getValue()))
                { // Field is checked
                    if (f.getFieldNameForDisplay() == "Year")
                    {
                        year = f.getValue().Trim();
                        if (int.Parse(year) < 1900)
                        {
                            MessageBox.Show(String.Format("Probleme avec l'année du champ \"{0}\"", field));
                            send = false;
                        }
                    }
                    else if (f.getFieldNameForDisplay() == "Month")
                    {
                        month = f.getValue().Trim();
                        if (int.Parse(month) > 12)
                        {
                            MessageBox.Show(String.Format("Probleme avec le mois du champ \"{0}\"", field));
                            send = false;
                        }
                    }
                    else
                    {
                        day = f.getValue().Trim();
                        if (int.Parse(day) > 31)
                        {
                            MessageBox.Show(String.Format("Probleme avec le jour du champ \"{0}\"", field));
                            send = false;
                        }
                    }

                }
                else
                {
                    MessageBox.Show(String.Format("Veuillez entrer la date pour \"{0}\"", field));
                    send = false;
                    EmptyFieldsCollector(f.getFieldNameForDisplay());
                    return null;
                }
            }

            return String.Format("{0}-{1}-{2}", year, month, day);
        }

        /// <summary>
        /// Get the data from bookmark in the Word document
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <param name="bookmark">The bookmark of the data we wanna retreive</param>
        /// <param name="field">The name of the field we are retreiving, used solely for error displaying</param>
        /// <returns>Returns the data as a string</returns>
        public string getDropdown(Word.Document TheDoc, string bookmark, string field)
        { //if default option "Choisissez un element" is selected, it will not cause an error
            TDHFormField dropdown = new TDHFormField(TheDoc, bookmark, field, mandatoryPrefix, "dropdown");

            if (dropdown.isOk())
            {
                return dropdown.getValue();
            }
            else
            {
                MessageBox.Show(String.Format("Veuillez entrer \"{0}\"", field));
                send = false;
                EmptyFieldsCollector(dropdown.getFieldNameForDisplay());
                return null;
            }
        }

        /// <summary>
        /// Get the checkbox's state from bookmark in the Word document
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <param name="bookmark">The bookmark of the checkbox we wanna retreive</param>
        /// <param name="field">The name of the field we are retreiving, used solely for error displaying</param>
        /// <returns>Returns the state of the checkbox as an int (1 is checked, 0 is unchecked, -1 is error)</returns>
        public int getCheckbox(Word.Document TheDoc, string bookmark, string field)
        {
            TDHFormField checkBox = new TDHFormField(TheDoc, bookmark, field, mandatoryPrefix, "checkbox");
            if (checkBox.isOk())
            {
                if (checkBox.isChecked())
                {
                    return 1; //if checked
                }
                else
                {
                    return 0; //if not checked
                }
            }
            else
            {
                MessageBox.Show(String.Format("Probleme avec la checkbox \"{0}\"", field));
                send = false;
                EmptyFieldsCollector(field);
                return -1;
            }
        }

        /// <summary>
        /// Get the checkboxes' state from bookmark in the Word document
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <param name="bookmark">The bookmark of the checkbox we wanna retreive</param>
        /// <param name="field">The name of the field we are retreiving, used solely for error displaying</param>
        /// <returns>Returns the state of the checkbox as an int (1 is yes checked, 0 is no checked, -1 is error)</returns>
        public int getYesNoCheckboxes(Word.Document TheDoc, string bookmark, string field)
        {
            int checkboxChecked = 0;
            string yesNo = null;

            TDHFormField[] checkBoxes = { new TDHFormField(TheDoc, bookmark+"_yes_checkbox", field+"_yes", mandatoryPrefix, "checkbox"),
                                        new TDHFormField(TheDoc, bookmark+"_no_checkbox", field+"_no", mandatoryPrefix, "checkbox") };

            foreach (TDHFormField checkbox in checkBoxes)
            {
                if (checkbox.isOk())
                {
                    if (checkbox.isChecked())
                    {
                        checkboxChecked++;
                        if (checkbox.getFieldNameForDisplay() == field + "_yes")
                        {
                            yesNo = "Yes";
                        }
                        else if (checkbox.getFieldNameForDisplay() == field + "_no")
                        {
                            yesNo = "No";
                        }
                    }
                }
                else
                {
                    MessageBox.Show(String.Format("Probleme avec les checkboxes \"{0}\"", field));
                    send = false;
                    return -1;
                }
            }

            if (checkboxChecked == 1)
            {
                if (yesNo == "Yes")
                {
                    return 1;
                }
                else if (yesNo == "No")
                {
                    return 0;
                }
                else
                {
                    MessageBox.Show(String.Format("Probleme avec les checkboxes \"{0}\"", field));
                    send = false;
                    return -1;
                }
            }
            else if (checkboxChecked == 0)
            {
                MessageBox.Show(String.Format("Probleme avec les checkboxes \"{0}\": veuillez cocher une case", field));
                send = false;
                return -1;
            }
            else
            {
                MessageBox.Show(String.Format("Probleme avec les checkboxes \"{0}\": veuillez cocher une seule case", field));
                send = false;
                EmptyFieldsCollector(field);
                return -1;
            }
        }

        /// <summary>
        /// Get the numeric value from bookmark in the Word document
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <param name="bookmark">The bookmark of the checkbox we wanna retreive</param>
        /// <param name="field">The name of the field we are retreiving, used solely for error displaying</param>
        /// <returns>Returns the value from the field as an int</returns>
        public int getInt(Word.Document TheDoc, string bookmark, string field)
        {
            TDHFormField intField = new TDHFormField(TheDoc, bookmark, mandatoryPrefix, field);

            if (intField.isOk())
            {
                if (!String.IsNullOrWhiteSpace(intField.getValue()))
                {
                    int number = 0;
                    try
                    {
                        number = int.Parse(intField.getValue());
                    }
                    catch
                    {
                        send = false;
                        MessageBox.Show(String.Format("Probleme avec la lecture du nombre dans le champ \"{0}\"", field));
                        EmptyFieldsCollector(intField.getFieldNameForDisplay());
                        return 0;
                    }

                    return number;

                }
                else
                {
                    return 0;
                }

            }
            else
            {
                MessageBox.Show(String.Format("Probleme avec le champs \"{0}\"", field));
                send = false;
                return 0;
            }
        }

        /// <summary>
        /// Get the numeric value from bookmark in the Word document
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <param name="bookmark">The bookmark of the checkbox we wanna retreive</param>
        /// <param name="field">The name of the field we are retreiving, used solely for error displaying</param>
        /// <returns>Returns the value from the field as a double</returns>
        public double getDouble(Word.Document TheDoc, string bookmark, string field)
        {
            TDHFormField doubleField = new TDHFormField(TheDoc, bookmark, mandatoryPrefix, field);

            if (doubleField.isOk())
            {
                if (!String.IsNullOrWhiteSpace(doubleField.getValue()))
                {
                    double number = 0;
                    try
                    {
                        number = double.Parse(doubleField.getValue());
                    }
                    catch
                    {
                        send = false;
                        MessageBox.Show(String.Format("Probleme avec la lecture du nombre dans le champ \"{0}\"", field));
                        EmptyFieldsCollector(doubleField.getFieldNameForDisplay());
                        return -1;
                    }

                    return number;

                }
                else
                {
                    return 0;
                }

            }
            else
            {
                MessageBox.Show(String.Format("Probleme avec le champs \"{0}\"", field));
                send = false;
                return 0;
            }
        }

        /// <summary>
        /// Get the value from bookmark in the Word document
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <param name="bookmark">The bookmark of the checkbox we wanna retreive</param>
        /// <param name="field">The name of the field we are retreiving, used solely for error displaying</param>
        /// <returns>Returns the value from the field as a string</returns>
        public string getString(Word.Document TheDoc, string bookmark, string field)
        {
            TDHFormField stringField = new TDHFormField(TheDoc, bookmark, field, mandatoryPrefix);

            if (stringField.isOk())
            {
                if (!String.IsNullOrWhiteSpace(stringField.getValue()))
                {
                    return stringField.getValue();
                }
                else
                {
                    return null;
                }

            }
            else
            {
                MessageBox.Show(String.Format("Probleme avec le champ \"{0}\"", field));
                send = false;
                EmptyFieldsCollector(stringField.getFieldNameForDisplay());
                return null;
            }
        }

        /// <summary>
        /// Returns the severity level of the case in the Word document based on checkboxes
        /// </summary>
        /// <param name="TheDoc">The Word document used to fill in the reports</param>
        /// <returns>Returns the seriousness of the case as an int (1 is low, 2 is medium, 3 is high, -1 is error)</returns>
        private int seriousnessCheck(Word.Document TheDoc)
        {
            int valid = 0; // to check validity of "radio button" select

            // Seriousness level
            TDHFormField[] serLevel = { new TDHFormField(TheDoc, "gravity_low_checkbox", "Faible", mandatoryPrefix, "checkbox"),
                                        new TDHFormField(TheDoc, "gravity_medium_checkbox","Moyenne", mandatoryPrefix, "checkbox"),
                                        new TDHFormField(TheDoc, "gravity_high_checkbox","Haute", mandatoryPrefix, "checkbox") };

            string seriousness = null;

            foreach (TDHFormField f in serLevel)
            { // Let's see which field is checked
                if (f.isOk() && f.isChecked())
                { // Field is checked
                    seriousness = f.getFieldNameForDisplay();
                    valid++;
                }
            }

            if (valid < 1)
            {
                MessageBox.Show("Erreur de définition de la gravité: veuillez cocher une case");
                send = false;
                return -1;
            }
            else if (valid > 1)
            {
                MessageBox.Show("Erreur de définition de la gravité: veuillez cocher une seule case");
                send = false;
                return -1;
            }

            if (seriousness == "Faible")
            {
                return 1;
            }
            else if (seriousness == "Moyenne")
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }
        //Permet d'obtenir la valeur selectionnée dans la liste déroulante depuis une requete SQL,
        //Met la valeur a -1 si aucune valeur n'a été trouvée.
        //Le tag de la liste
        //
        private string getValeur(Word.Document TheDoc, string ListeNom)
        {
            //OdbcConnection donnee = new OdbcConnection(MyConString);
            //donnee.Open();

            //for (int i = 1; i <= TheDoc.ContentControls.Count; i++)
            //{
            //    if (TheDoc.ContentControls[i].Type == Word.WdContentControlType.wdContentControlComboBox && TheDoc.ContentControls[i].Tag == ListeNom)
            //    {
            //        string requete = "SELECT FK_Listes, FK_Langues, Valeur, Traduction FROM Traductions WHERE Traduction = '" + TheDoc.ContentControls[i].Range.Text + "'";
            //        using (OdbcCommand cmd = new OdbcCommand(requete, donnee))
            //        {
            //            OdbcDataReader reader = cmd.ExecuteReader();
            //            if (reader.HasRows)
            //            {
            //                while (reader.Read())
            //                {
            //                    return reader.GetString(2);
            //                }
            //            }
            //            reader.Close();
            //        }
            //    }
            //}
            return "";

        }
        //
        //
        //String En argument, il doit être le même que le nom de la box sur le fichier Word.
        //Methode permettant de prendre le contennu des Box ActiveX
        private string getTextBox(Word.Document TheDoc, string NomBox)
        {
            for (int i = 1; i < TheDoc.Shapes.Count; i++)
            {

                if (TheDoc.Shapes[i].OLEFormat.Object.Name == NomBox)
                {
                    string s = TheDoc.Shapes[i].OLEFormat.Object.Value;
                    if (s == null)
                    {
                        return "";
                    }
                    return s.Replace("'", "''");

                }
            }
            return "";
        }

        //Permet d'obtenir la liste selectionnée dans le document une requete SQL,
        //Met la valeur a -1 si aucune valeur n'a été trouvée.
        //Depuis le tag de la liste
        //
        private String getListe(Word.Document TheDoc, string ListeNom)
        {
            OdbcConnection donnee = new OdbcConnection(MyConString);
            donnee.Open();

            for (int i = 1; i <= TheDoc.ContentControls.Count; i++)
            {
                if (TheDoc.ContentControls[i].Type == Word.WdContentControlType.wdContentControlComboBox && TheDoc.ContentControls[i].Tag == ListeNom)
                {
                    string requete = "SELECT FK_Listes, FK_Langues, Valeur, Traduction FROM Traductions WHERE Traduction = '" + TheDoc.ContentControls[i].Range.Text + "'";
                    using (OdbcCommand cmd = new OdbcCommand(requete, donnee))
                    {
                        OdbcDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                return reader.GetString(0);
                            }
                        }
                        reader.Close();
                    }
                }
            }
            return "";
        }
        #endregion Word accessors (get fields value)

        #region Input Validation
        //Collect all fields dectected as mandatory and empty
        private void EmptyFieldsCollector(string bookmarkName)
        {
            if (bookmarkName.Substring(0, mandatoryPrefix.Length-1) == mandatoryPrefix)
            {
                if (this.listOfMandatoryBookmarkNameEmpty == null)
                {
                    //we initizialize the list
                    this.listOfMandatoryBookmarkNameEmpty = new List<string>();
                }
                //we add the bookmark in the list
                this.listOfMandatoryBookmarkNameEmpty.Add(bookmarkName);
            }
        }

        //Build and display the message containing all mandatory fields without value (value missing)
        //This method must be called only if the listOfMandatoryBookmarsNameEmpty is different as null
        private void DisplayEmptyMandatoryFields()
        {
            if (listOfMandatoryBookmarkNameEmpty != null)
            {
                string msgToDisplay = "Les champs obligatoires suivants sont manquants ou vides" + "/r/n" + "--------------/r/n";

                foreach (String emptyBookmarks in listOfMandatoryBookmarkNameEmpty)
                {
                    msgToDisplay += emptyBookmarks + "/r/n";
                }
            }
        }
        #endregion Input Validation

    }
}

