﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data;

namespace TDH
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.DocumentOpen += Application_DocumentOpen; // XCL: tells the application to call method Application_DocumentOpen when a document is opened
        }

        private void Application_DocumentOpen(Word.Document Doc)
        {
            /*
            Word.ContentControl contentControl = null;
            Word.ContentControls contentControls = Doc.ContentControls; // XCL: List of all content controls (= controls inside the document, as opposed to those of the ribbon)
            for (int i = 1; i <= contentControls.Count; i++) // XCL: Loop through them
            {
                contentControl = contentControls[i];
                if (contentControl.Type == Word.WdContentControlType.wdContentControlComboBox && contentControl.Tag == "IDZone") // XCL: One dropdownlist of interest, identified by its tag ("Balise")
                {
                    // XCL: Initialise the dropdown content. Here I hardcoded some values. They should come from the database
                    contentControl.DropdownListEntries.Clear();
                    contentControl.DropdownListEntries.Add("Europe", "1", 1); // XCL: Add method parameters are (Display, Value, Index). Index must be unique
                    contentControl.DropdownListEntries.Add("Asie", "2", 2);
                    contentControl.DropdownListEntries.Add("Amérique Nord", "3", 3);
                    contentControl.DropdownListEntries.Add("Amérique Sud", "4", 4);
                    contentControl.DropdownListEntries.Add("Océanie", "5", 5);
                }
            }

    */
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
